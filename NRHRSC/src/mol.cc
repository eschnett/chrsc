#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace NRHRSC {

extern "C" void NRHRSC_register(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int cgroup = CCTK_GroupIndex("NRHRSC::conserved");
  if (cgroup < 0)
    CCTK_ERROR("Internal error");
  int cgrouprhs = CCTK_GroupIndex("NRHRSC::conserved_rhs");
  if (cgrouprhs < 0)
    CCTK_ERROR("Internal error");
  int ierr = MoLRegisterEvolvedGroup(cgroup, cgrouprhs);
  if (ierr < 0)
    CCTK_ERROR("Internal error");
}
}
