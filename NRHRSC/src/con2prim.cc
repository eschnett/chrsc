#include "state.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

namespace NRHRSC {

extern "C" void NRHRSC_con2prim(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel
  CCTK_LOOP3_ALL(NRHRSC_con2prim, cctkGH, i, j, k) {
    int idx = CCTK_GFINDEX3D(cctkGH, i, j, k);
    auto bgnd = background_t<CCTK_REAL>(phi, idx);
    auto cons = conserved_t<CCTK_REAL>(dens, momx, momy, momz, etot, idx);
    auto prim = primitive_t<CCTK_REAL>(bgnd, cons);
    prim.normalize();
    cons = conserved_t<CCTK_REAL>(bgnd, prim);
    cons.store(dens, momx, momy, momz, etot, idx);
    prim.store(rho, velx, vely, velz, eps, press, idx);
  }
  CCTK_ENDLOOP3_ALL(NRHRSC_con2prim);
}
}
