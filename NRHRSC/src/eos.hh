#ifndef EOS_HH
#define EOS_HH

#include <cctk.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace NRHRSC {
using namespace std;

// internal energy:   dU = T dS - p dV
// enthalpy:          dH = T dS + V dp

// Common relations:
//    p = (gamma-1) eps
//    u = eps / rho
//    T = u / cV
//    gamma = cp / cV
//    v = 1/rho   ("specific volume")

// Enthalpy:
//    H = U + p V
//    h = u + p v
//    h rho = eps + p

// Speed of sound:
//    cs^2 = dp/drho | s=const
//    cs^2 = gamma p / rho   (ideal gas)

// Heat capacity (constant volume)
template <typename T> T eos_cV(T rho, T eps) {
  DECLARE_CCTK_PARAMETERS;
  const T R = eos_kB / (eos_mu * eos_amu);
  return R / (eos_gamma - 1);
}

// Heat capacity (constant pressure)
template <typename T> T eos_cp(T rho, T eps) {
  DECLARE_CCTK_PARAMETERS;
  return eos_gamma * eos_cV(rho, eps);
}

// Pressure
template <typename T> T eos_press(T rho, T eps) {
  DECLARE_CCTK_PARAMETERS;
  return (eos_gamma - 1) * eps;
}

// Temperature
template <typename T> T eos_temp(T rho, T eps) {
  return eps / rho / eos_cV(rho, eps);
  // T = u / cV
}

// Enthalpy density
template <typename T> T eos_enthalpy(T rho, T eps) {
  return eps + eos_press(rho, eps);
}

// Speed of sound
template <typename T> T eos_cs(T rho, T eps) {
  DECLARE_CCTK_PARAMETERS;
  T press = eos_press(rho, eps);
  return sqrt(eos_gamma * press / rho);
  // cs^2 = gamma (gamma-1) u
}
}

#endif // #ifndef EOS_HH
