#ifndef STATE_HH
#define STATE_HH

#include "defs.hh"
#include "eos.hh"

#include <cctk.h>

#include <array>
#include <cstddef>
#include <functional>

namespace NRHRSC {
using namespace std;

constexpr int dim = 3;

template <typename T> struct background_t;
template <typename T> struct conserved_t;
template <typename T> struct primitive_t;

template <typename T> struct background_t {
  T phi;

  background_t() = default;
  background_t(const background_t &) = default;
  background_t(background_t &&) = default;
  background_t &operator=(const background_t &) = default;
  background_t &operator=(background_t &&) = default;

  background_t(T phi) : phi(phi) {}

  background_t(const T *restrict phivar, ptrdiff_t idx) : phi(phivar[idx]) {}

  void store(T *restrict phivar, ptrdiff_t idx) const { phivar[idx] = phi; }

  static background_t<T> load(const background_t<const T * restrict> &vars,
                              ptrdiff_t idx) {
    T phi = vars.phi[idx];
    return {phi};
  }

  // if T is a container, e.g. array<X,N>
  template <typename T1 = T, typename U = typename T1::value_type>
  static background_t<T> load(const background_t<const U * restrict> &vars,
                              ptrdiff_t idx, ptrdiff_t stride) {
    return map(
        [=](const U *restrict var) {
          T res;
          for (size_t n = 0; n < res.size(); ++n)
            res[n] = var[idx + n * stride];
          return res;
        },
        vars);
  }

  // map
  template <typename F, typename... Args>
  static background_t map(const F &f, const background_t<Args> &... args) {
    background_t bgnd;
    bgnd.phi = f(args.phi...);
    return bgnd;
  }
};

template <typename T> struct conserved_t {
  T dens;
  array<T, dim> mom;
  T etot;

  conserved_t() = default;
  conserved_t(const conserved_t &) = default;
  conserved_t(conserved_t &&) = default;
  conserved_t &operator=(const conserved_t &) = default;
  conserved_t &operator=(conserved_t &&) = default;

  conserved_t(T dens, const array<T, dim> &mom, T etot)
      : dens(dens), mom(mom), etot(etot) {}

  conserved_t(const T *restrict densvar, const T *restrict momxvar,
              const T *restrict momyvar, const T *restrict momzvar,
              const T *restrict etotvar, ptrdiff_t idx)
      : dens(densvar[idx]), mom({momxvar[idx], momyvar[idx], momzvar[idx]}),
        etot(etotvar[idx]) {}

  void store(T *restrict densvar, T *restrict momxvar, T *restrict momyvar,
             T *restrict momzvar, T *restrict etotvar, ptrdiff_t idx) const {
    densvar[idx] = dens;
    momxvar[idx] = mom[0];
    momyvar[idx] = mom[1];
    momzvar[idx] = mom[2];
    etotvar[idx] = etot;
  }

  static conserved_t<T> load(const conserved_t<const T * restrict> &vars,
                             ptrdiff_t idx) {
    T dens = vars.dens[idx];
    array<T, dim> mom;
    for (int d = 0; d < dim; ++d)
      mom[d] = vars.mom[d][idx];
    T etot = vars.etot[idx];
    return {dens, mom, etot};
  }

  // if T is a container, e.g. array<X,N>
  template <typename T1 = T, typename U = typename T1::value_type>
  static conserved_t<T> load(const conserved_t<const U * restrict> &vars,
                             ptrdiff_t idx, ptrdiff_t stride) {
    return map(
        [=](const U *restrict var) {
          T res;
          for (size_t n = 0; n < res.size(); ++n)
            res[n] = var[idx + n * stride];
          return res;
        },
        vars);
  }

  void store(const conserved_t<T * restrict> &vars, ptrdiff_t idx) const {
    vars.dens[idx] = dens;
    for (int d = 0; d < dim; ++d)
      vars.mom[d][idx] = mom[d];
    vars.etot[idx] = etot;
  }

  // map
  template <typename F, typename... Args>
  static conserved_t map(const F &f, const conserved_t<Args> &... args) {
    conserved_t cons;
    cons.dens = f(args.dens...);
    for (int d = 0; d < dim; ++d)
      cons.mom[d] = f(args.mom[d]...);
    cons.etot = f(args.etot...);
    return cons;
  }

  // zero
  static conserved_t zero() {
    return map([] { return T(0); });
  }

  // plus
  conserved_t operator+(const conserved_t &x) const {
    return map(plus<T>(), *this, x);
  }
  conserved_t &operator+=(const conserved_t &x) { return *this = *this + x; }

  // prim2con
  conserved_t(const background_t<T> &bgnd, const primitive_t<T> &prim);

  conserved_t flux(const background_t<T> &bgnd, const primitive_t<T> &prim,
                   int dir) const;
};

template <typename T> struct primitive_t {
  T rho;
  array<T, dim> vel;
  T eps;
  T press;

  primitive_t() = default;
  primitive_t(const primitive_t &) = default;
  primitive_t(primitive_t &&) = default;
  primitive_t &operator=(const primitive_t &) = default;
  primitive_t &operator=(primitive_t &&) = default;

  primitive_t(T rho, const array<T, dim> &vel, T eps, T press)
      : rho(rho), vel(vel), eps(eps), press(press) {}

  primitive_t(T rho, const array<T, dim> &vel, T eps)
      : primitive_t(rho, vel, eps, eos_press(rho, eps)) {}

  primitive_t(const T *restrict rhovar, const T *restrict velxvar,
              const T *restrict velyvar, const T *restrict velzvar,
              const T *restrict epsvar, ptrdiff_t idx)
      : primitive_t(rhovar[idx], {velxvar[idx], velyvar[idx], velzvar[idx]},
                    epsvar[idx]) {}

  void store(T *restrict rhovar, T *restrict velxvar, T *restrict velyvar,
             T *restrict velzvar, T *restrict epsvar, T *restrict pressvar,
             ptrdiff_t idx) const {
    rhovar[idx] = rho;
    velxvar[idx] = vel[0];
    velyvar[idx] = vel[1];
    velzvar[idx] = vel[2];
    epsvar[idx] = eps;
    pressvar[idx] = press;
  }

  static primitive_t<T> load(const primitive_t<const T * restrict> &vars,
                             ptrdiff_t idx) {
    T rho = vars.rho[idx];
    array<T, dim> vel;
    for (int d = 0; d < dim; ++d)
      vel[d] = vars.vel[d][idx];
    T eps = vars.eps[idx];
    T press = vars.press[idx];
    return {rho, vel, eps, press};
  }

  // if T is a container, e.g. array<X,N>
  template <typename T1 = T, typename U = typename T1::value_type>
  static primitive_t<T> load(const primitive_t<const U * restrict> &vars,
                             ptrdiff_t idx, ptrdiff_t stride) {
    return map(
        [=](const U *restrict var) {
          T res;
          for (size_t n = 0; n < res.size(); ++n)
            res[n] = var[idx + n * stride];
          return res;
        },
        vars);
  }

  // map
  template <typename F, typename... Args>
  static primitive_t map(const F &f, const primitive_t<Args> &... args) {
    primitive_t prim;
    prim.rho = f(args.rho...);
    for (int d = 0; d < dim; ++d)
      prim.vel[d] = f(args.vel[d]...);
    prim.eps = f(args.eps...);
    prim.press = f(args.press...);
    return prim;
  }

  // con2prim
  primitive_t(const background_t<T> &bgnd, const conserved_t<T> &cons);

  // normalize
  void normalize();
};

template <typename T>
conserved_t<T>::conserved_t(const background_t<T> &bgnd,
                            const primitive_t<T> &prim) {
  dens = prim.rho;
  for (int d = 0; d < dim; ++d)
    mom[d] = prim.rho * prim.vel[d];
  T ekin = 0;
  for (int d = 0; d < dim; ++d)
    ekin += square(prim.vel[d]);
  ekin *= 0.5 * prim.rho;
  etot = ekin + prim.eps;
}

template <typename T>
conserved_t<T> conserved_t<T>::flux(const background_t<T> &bgnd,
                                    const primitive_t<T> &prim, int dir) const {
  conserved_t flux;
  flux.dens = dens * prim.vel[dir];
  for (int d = 0; d < dim; ++d)
    flux.mom[d] = mom[d] * prim.vel[dir] + (d == dir) * prim.press;
  flux.etot = (etot + prim.press) * prim.vel[dir];
  return flux;
}

template <typename T>
primitive_t<T>::primitive_t(const background_t<T> &bgnd,
                            const conserved_t<T> &cons) {
  rho = cons.dens;
  for (int d = 0; d < dim; ++d)
    vel[d] = cons.mom[d] / rho;
  T ekin = 0;
  for (int d = 0; d < dim; ++d)
    ekin += square(vel[d]);
  ekin *= 0.5 * rho;
  eps = cons.etot - ekin;
  press = eos_press(rho, eps);
}

template <typename T> void primitive_t<T>::normalize() {
  DECLARE_CCTK_PARAMETERS;
  rho = fmax(rho_min, rho);
  eps = fmax(eps_min, eps);
  press = eos_press(rho, eps);
}
}

#endif // #ifndef STATE_HH
