#include "state.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>

namespace NRHRSC {
using namespace std;

// TODO: To implement gravity that works (the code currently doesn't
// work), look at Käppeli, Mishra: "Well-balanced schemes for the
// Euler equations with gravitation", Research Report No. 2013-05,
// Seminar für Angewandte Mathematik, Eidgenössische Technische
// Hochschule,
// <https://www.sam.math.ethz.ch/sam_reports/reports_final/reports2013/2013-05.pdf>.

// For angular momentum conservation, see also Käppeli, Mishra:
// "Structure-preserving schemes", Research Report No. 2014-02,
// Seminar für Angewandte Mathematik, Eidgenössische Technische
// Hochschule,
// <https://www.sam.math.ethz.ch/sam_reports/reports_final/reports2014/2014-02.pdf>.



// gravitational potential:
//    Phi(x) = alpha x^2
// pressure:
//    p = K rho^gamma
// central conditions:
//    given: rho, eps
//    K = eps / rho^gamma
// hydrodynamic equilibrium:
//    rho grad phi + grad p = 0
//    rho(x) = [rho_c^(gamma-1) - (gamma-1)/gamma alpha/K x^2] ^ (1/(gamma-1))
// stellar radius:
//    rho(x_r) = 0
//    x_r = sqrt[K/alpha gamma/(gamma-1) rho_c^(gamma-1)]
//    alpha = K gamma/(gamma-1) rho_c^(gamma-1) / x_r^2
// combined:
//    rho(x) = (1 - x^2/x_r^2) ^ (1/(gamma-1)) rho_c
//    alpha = gamma/(gamma-1) eps_c/rho_c 1/x_r^2

template <typename T> pair<background_t<T>, primitive_t<T> > star1d(T x) {
  DECLARE_CCTK_PARAMETERS;
  T alpha = eos_gamma * eps_central / (rho_central * square(stellar_radius));
  T phi =
      fabs(x) < stellar_radius
          ? alpha * square(x)
          : alpha * square(stellar_radius) +
                copysign(
                    (fabs(x) - stellar_radius) * 2 * alpha * stellar_radius, x);

  T rho = fabs(x) < stellar_radius
              ? rho_central *
                    pow(1 - square(x / stellar_radius), 1 / (eos_gamma - 1))
              : 0;
  array<T, dim> vel = {0, 0, 0};
  T K = (eos_gamma - 1) * eps_central / pow(rho_central, eos_gamma);
  T press = K * pow(rho, eos_gamma);
  T eps = press / (eos_gamma - 1);

  return {background_t<T>(phi), primitive_t<T>(rho, vel, eps)};
}

extern "C" void NRHRSC_star1d(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel
  CCTK_LOOP3_ALL(NRHRSC_star1d, cctkGH, i, j, k) {
    int idx = CCTK_GFINDEX3D(cctkGH, i, j, k);
    auto init = star1d<CCTK_REAL>(x[idx]);
    auto bgnd = init.first;
    auto prim = init.second;
    bgnd.store(phi, idx);
    prim.store(rho, velx, vely, velz, eps, press, idx);
  }
  CCTK_ENDLOOP3_ALL(NRHRSC_star1d);
}
}
