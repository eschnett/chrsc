ActiveThorns = "
    Boundary
    Carpet
    CarpetIOASCII
    CarpetIOBasic
    CarpetIOHDF5
    CarpetIOScalar
    CarpetLib
    CarpetReduce
    CartGrid3D
    CoordBase
    Formaline
    IOUtil
    MoL
    NRHRSC
    SymBase
    SystemTopology
    Time
    TimerReport
"

Cactus::info_format = "human-readable time stamp"



# Grid setup

Carpet::domain_from_coordbase = yes
CartGrid3D::type = "coordbase"

CoordBase::domainsize = "minmax"
CoordBase::spacing = "numcells"

$ncells = 1024

CoordBase::xmin = -1.0
CoordBase::ymin = -1.0/$ncells
CoordBase::zmin = -1.0/$ncells
CoordBase::xmax = +1.0
CoordBase::ymax = +1.0/$ncells
CoordBase::zmax = +1.0/$ncells

CoordBase::ncells_x = $ncells
CoordBase::ncells_y = 1
CoordBase::ncells_z = 1

CoordBase::boundary_staggered_x_lower = yes
CoordBase::boundary_staggered_y_lower = yes
CoordBase::boundary_staggered_z_lower = yes
CoordBase::boundary_staggered_x_upper = yes
CoordBase::boundary_staggered_y_upper = yes
CoordBase::boundary_staggered_z_upper = yes

CoordBase::boundary_size_x_lower = 2
CoordBase::boundary_size_y_lower = 2
CoordBase::boundary_size_z_lower = 2
CoordBase::boundary_size_x_upper = 2
CoordBase::boundary_size_y_upper = 2
CoordBase::boundary_size_z_upper = 2

driver::ghost_size_x = 2
driver::ghost_size_y = 2
driver::ghost_size_z = 2



# Evolution

NRHRSC::scenario = "shocktube"

NRHRSC::eos_gamma = 1.4

NRHRSC::rho_left = 1.0
NRHRSC::rho_right = 0.125
NRHRSC::eps_left = 2.5
NRHRSC::eps_right = 0.25

NRHRSC::reconstruction = "tvd"
NRHRSC::numeric_flux = "hlle"

NRHRSC::flux_boundary = "flat"
NRHRSC::rhs_boundary = "flat"

Time::dtfac = 0.25

cactus::cctk_itlast = $ncells

Carpet::poison_new_timelevels = no
MoL::init_rhs_zero = no

MoL::ODE_Method = "RK2-central"
MoL::MoL_Intermediate_Steps = 2
MoL::MoL_Num_Scratch_Levels = 1



# Output

IO::out_dir = $parfile

IOBasic::outInfo_every = $ncells/16
IOBasic::outInfo_reductions = "minimum maximum"
IOBasic::outInfo_vars = "
    NRHRSC::velx NRHRSC::press
"

IOScalar::one_file_per_group = yes
IOScalar::outScalar_every = $ncells/16
IOScalar::outScalar_vars = "
    NRHRSC::primitive
    NRHRSC::conserved
    NRHRSC::conserved_flux
    NRHRSC::conserved_rhs
"

IOASCII::one_file_per_group = yes
IOASCII::out1D_every = $ncells/16
IOASCII::out1D_vars = "
    NRHRSC::primitive
    NRHRSC::conserved
    NRHRSC::conserved_flux
    NRHRSC::conserved_rhs
"

IOHDF5::one_file_per_group = yes
IOHDF5::out3D_every = 0
IOHDF5::out3D_vars = "
    NRHRSC::primitive
    NRHRSC::conserved
    NRHRSC::conserved_flux
    NRHRSC::conserved_rhs
"



# Timing

TimerReport::out_every = 0
TimerReport::n_top_timers = 40
TimerReport::out_filename = "TimerReport"
TimerReport::output_all_timers = yes
