#include "state.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <utility>

namespace NRHRSC {
using namespace std;

template <typename T> pair<background_t<T>, primitive_t<T> > shocktube(T x) {
  DECLARE_CCTK_PARAMETERS;
  T phi = 0;
  bool isleft = x < 0;
  T rho = isleft ? rho_left : rho_right;
  array<T, dim> vel = {0, 0, 0};
  T eps = isleft ? eps_left : eps_right;
  return {background_t<T>(phi), primitive_t<T>(rho, vel, eps)};
}

extern "C" void NRHRSC_shocktube(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel
  CCTK_LOOP3_ALL(NRHRSC_shocktube, cctkGH, i, j, k) {
    int idx = CCTK_GFINDEX3D(cctkGH, i, j, k);
    auto init = shocktube<CCTK_REAL>(x[idx]);
    auto bgnd = init.first;
    auto prim = init.second;
    bgnd.store(phi, idx);
    prim.store(rho, velx, vely, velz, eps, press, idx);
  }
  CCTK_ENDLOOP3_ALL(NRHRSC_shocktube);
}
}
