#ifndef DEFS_HH
#define DEFS_HH

#include <cctk.h>
#include <cctk_Parameters.h>

namespace NRHRSC {

template <typename T> T square(T x) { return x * x; }
}

#endif // #ifndef DEFS_HH
