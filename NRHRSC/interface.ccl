# Interface definition for thorn NRHRSC

IMPLEMENTS: NRHRSC

INHERITS: grid



CCTK_INT FUNCTION Boundary_SelectGroupForBC \
  (CCTK_POINTER_TO_CONST IN cctkGH, \
   CCTK_INT IN faces, \
   CCTK_INT IN boundary_width, \
   CCTK_INT IN table_handle, \
   CCTK_STRING IN group_name, \
   CCTK_STRING IN bc_name)
REQUIRES FUNCTION Boundary_SelectGroupForBC

CCTK_INT FUNCTION GetBoundarySizesAndTypes \
  (CCTK_POINTER_TO_CONST IN cctkGH, \
   CCTK_INT IN size, \
   CCTK_INT OUT ARRAY bndsize, \
   CCTK_INT OUT ARRAY is_ghostbnd, \
   CCTK_INT OUT ARRAY is_symbnd, \
   CCTK_INT OUT ARRAY is_physbnd)
REQUIRES FUNCTION GetBoundarySizesAndTypes

CCTK_INT FUNCTION GetBoundarySpecification \
  (CCTK_INT IN size, \
   CCTK_INT OUT ARRAY nboundaryzones, \
   CCTK_INT OUT ARRAY is_internal, \
   CCTK_INT OUT ARRAY is_staggered, \
   CCTK_INT OUT ARRAY shiftout)
REQUIRES FUNCTION GetBoundarySpecification

CCTK_INT FUNCTION MoLRegisterEvolvedGroup \
  (CCTK_INT IN EvolvedIndex, \
   CCTK_INT IN RHSIndex)
REQUIRES FUNCTION MoLRegisterEvolvedGroup




CCTK_REAL background TYPE=gf
{
  phi
} "Background quantities"

CCTK_REAL conserved TYPE=gf TIMELEVELS=2
{
  dens
  momx momy momz
  etot
} "Conserved quantities"

CCTK_REAL primitive TYPE=gf
{
  rho
  velx vely velz
  eps
  press
} "Primitive quantities"

CCTK_REAL conserved_flux TYPE=gf
{
  dens_flux_x
  momx_flux_x momy_flux_x momz_flux_x
  etot_flux_x
  dens_flux_y
  momx_flux_y momy_flux_y momz_flux_y
  etot_flux_y
  dens_flux_z
  momx_flux_z momy_flux_z momz_flux_z
  etot_flux_z
} "Flux of conserved quantities"

CCTK_REAL conserved_rhs TYPE=gf
{
  dens_rhs
  momx_rhs momy_rhs momz_rhs
  etot_rhs
} "RHS of conserved quantities"
