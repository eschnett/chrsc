#include "state.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <array>
#include <cassert>

namespace NRHRSC {
using namespace std;

template <typename T> T deriv(const array<T, 2> &vals) {
  return vals[1] - vals[0];
}

extern "C" void NRHRSC_rhs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const int di =
      CCTK_GFINDEX3D(cctkGH, 1, 0, 0) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const int dj =
      CCTK_GFINDEX3D(cctkGH, 0, 1, 0) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const int dk =
      CCTK_GFINDEX3D(cctkGH, 0, 0, 1) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const array<int, dim> strides{di, dj, dk};

  const CCTK_REAL dx = CCTK_DELTA_SPACE(0);
  const CCTK_REAL dy = CCTK_DELTA_SPACE(1);
  const CCTK_REAL dz = CCTK_DELTA_SPACE(2);
  const array<CCTK_REAL, dim> spacings{dx, dy, dz};

  const primitive_t<const CCTK_REAL * restrict> prim_vars{
      rho, {velx, vely, velz}, eps, press};

  const array<conserved_t<const CCTK_REAL * restrict>, dim> flux_vars{
      {{dens_flux_x, {momx_flux_x, momy_flux_x, momz_flux_x}, etot_flux_x},
       {dens_flux_y, {momx_flux_y, momy_flux_y, momz_flux_y}, etot_flux_y},
       {dens_flux_z, {momx_flux_z, momy_flux_z, momz_flux_z}, etot_flux_z}}};

  const conserved_t<CCTK_REAL * restrict> rhs_vars{
      dens_rhs, {momx_rhs, momy_rhs, momz_rhs}, etot_rhs};

#pragma omp parallel
  CCTK_LOOP3_INT(NRHRSC_rhs, cctkGH, i, j, k) {
    int idx = CCTK_GFINDEX3D(cctkGH, i, j, k);

    auto rhs = conserved_t<CCTK_REAL>::zero();

    // Flux divergence
    for (int dir = 0; dir < dim; ++dir) {
      auto flux_array = conserved_t<array<CCTK_REAL, 2> >::load(
          flux_vars[dir], idx, strides[dir]);
      rhs += conserved_t<CCTK_REAL>::map(
          [&](const array<CCTK_REAL, 2> &vals) {
            return -deriv(vals) / spacings[dir];
          },
          flux_array);
    }

    // Source terms

    // Gravity:
    // mom[i] += - rho grad[i] Phi
    // etot += - rho vel[i] grad[i] Phi
    array<CCTK_REAL, dim> grad_phi;
    for (int d = 0; d < dim; ++d)
      grad_phi[d] =
          (phi[idx + strides[d]] - phi[idx - strides[d]]) / (2 * spacings[d]);

    auto prim = primitive_t<CCTK_REAL>::load(prim_vars, idx);

    for (int d = 0; d < dim; ++d)
      rhs.mom[d] -= prim.rho * grad_phi[d];

    CCTK_REAL vel_grad_phi = 0;
    for (int d = 0; d < dim; ++d)
      vel_grad_phi += prim.vel[d] * grad_phi[d];
    rhs.etot -= prim.rho * vel_grad_phi;

    rhs.store(rhs_vars, idx);
  }
  CCTK_ENDLOOP3_INT(NRHRSC_rhs);
}

extern "C" void NRHRSC_rhs_SelectBCs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT nboundaryzones[6], is_internal[6], is_staggered[6], shiftout[6];
  int ierr = GetBoundarySpecification(6, nboundaryzones, is_internal,
                                      is_staggered, shiftout);
  if (ierr < 0)
    CCTK_ERROR("Internal error");
  for (int d = 1; d < 6; ++d)
    if (nboundaryzones[d] != nboundaryzones[0])
      CCTK_ERROR("Internal error");

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, nboundaryzones[0],
                                   -1, "NRHRSC::conserved_rhs", rhs_boundary);
  if (ierr < 0)
    CCTK_ERROR("Internal error");
}
}
