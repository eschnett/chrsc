#include "state.hh"

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cassert>
#include <cmath>

namespace NRHRSC {
using namespace std;

enum limiter_t { lim_error, lim_minmod, lim_vanleer, lim_mc2, lim_superbee };

// Minmod slope limiter
template <typename T> T minmod(T a, T b) {
  return (copysign(0.5, a) + copysign(0.5, b)) * fmin(fabs(a), fabs(b));
}

// Van Leer slope limiter
template <typename T> T vanleer(T a, T b) {
  // return fmax(T(0), a * b) * 2 / (a + b);
  return a * b > 0 ? 2 * a * b / (a + b) : T(0);
}

// MC2 slope limiter
template <typename T> T mc2(T a, T b) {
  return (copysign(0.25, a) + copysign(0.25, b)) *
         fmin(4 * fmin(fabs(a), fabs(b)), fabs(a) + fabs(b));
}

// Superbee slope limiter
template <typename T> T superbee(T a, T b) {
  return (copysign(0.5, a) + copysign(0.5, b)) *
         fmax(fmin(2 * fabs(a), fabs(b)), fmin(fabs(a), 2 * fabs(b)));
}

enum recon_t { recon_error, recon_pc, recon_tvd };

// PC (piecewise constant) reconstruction
template <typename T> T pc(const array<T, 4> &vals, int face) {
  if (face == 0) {
    return vals[2];
  } else {
    return vals[1];
  }
}

// TVD (total variation diminishing) reconstruction
template <typename T> T tvd(const array<T, 4> &vals, int face) {
  if (face == 0) {
    T delta = minmod(vals[2] - vals[1], vals[3] - vals[2]);
    return vals[2] - 0.5 * delta;
  } else {
    T delta = minmod(vals[1] - vals[0], vals[2] - vals[1]);
    return vals[1] + 0.5 * delta;
  }
}

enum num_flux_t { num_flux_error, num_flux_llf, num_flux_hlle };

// LLF (local Lax-Friedrichs) Riemann solver
template <typename T>
T llf(T lambda_m, T lambda_p, T var_m, T var_p, T flux_m, T flux_p) {
  return 0.5 * (flux_m + flux_p) -
         0.5 * fmax(fabs(lambda_m), fabs(lambda_p)) * (var_m - var_p);
}

// HLLE (Harten, Lax, van Leer, Einfeldt) Riemann solver
template <typename T>
T hlle(T lambda_m, T lambda_p, T var_m, T var_p, T flux_m, T flux_p) {
  return (lambda_p * flux_p - lambda_m * flux_m +
          lambda_p * lambda_m * (var_m - var_p)) /
         (lambda_p - lambda_m);
}

extern "C" void NRHRSC_flux(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  const recon_t recon = CCTK_EQUALS(reconstruction, "pc")
                            ? recon_pc
                            : CCTK_EQUALS(reconstruction, "tvd")
                                  ? recon_tvd
                                  : (assert(0), recon_error);
  const auto reconstruct = [&](const array<CCTK_REAL, 4> &vals, int face) {
    switch (recon) {
    case recon_pc:
      return pc(vals, face);
    case recon_tvd:
      return tvd(vals, face);
    default:
      CCTK_BUILTIN_UNREACHABLE();
    }
  };

  const num_flux_t num_flux = CCTK_EQUALS(numeric_flux, "llf")
                                  ? num_flux_llf
                                  : CCTK_EQUALS(numeric_flux, "hlle")
                                        ? num_flux_hlle
                                        : (assert(0), num_flux_error);
  const auto get_numeric_flux = [&](CCTK_REAL lambda_m, CCTK_REAL lambda_p,
                                    CCTK_REAL var_m, CCTK_REAL var_p,
                                    CCTK_REAL flux_m, CCTK_REAL flux_p) {
    switch (num_flux) {
    case num_flux_llf:
      return llf(lambda_m, lambda_p, var_m, var_p, flux_m, flux_p);
    case num_flux_hlle:
      return hlle(lambda_m, lambda_p, var_m, var_p, flux_m, flux_p);
    default:
      CCTK_BUILTIN_UNREACHABLE();
    }
  };

  const int di =
      CCTK_GFINDEX3D(cctkGH, 1, 0, 0) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const int dj =
      CCTK_GFINDEX3D(cctkGH, 0, 1, 0) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const int dk =
      CCTK_GFINDEX3D(cctkGH, 0, 0, 1) - CCTK_GFINDEX3D(cctkGH, 0, 0, 0);
  const array<int, dim> strides{di, dj, dk};

  const background_t<const CCTK_REAL * restrict> bgnd_vars{phi};
  const primitive_t<const CCTK_REAL * restrict> prim_vars{
      rho, {velx, vely, velz}, eps, press};

  const array<conserved_t<CCTK_REAL * restrict>, dim> flux_vars{
      {{dens_flux_x, {momx_flux_x, momy_flux_x, momz_flux_x}, etot_flux_x},
       {dens_flux_y, {momx_flux_y, momy_flux_y, momz_flux_y}, etot_flux_y},
       {dens_flux_z, {momx_flux_z, momy_flux_z, momz_flux_z}, etot_flux_z}}};

#pragma omp parallel
  CCTK_LOOP3_INT(NRHRSC_flux, cctkGH, i, j, k) {
    int idx = CCTK_GFINDEX3D(cctkGH, i, j, k);

    for (int dir = 0; dir < dim; ++dir) {
      int stride = strides[dir];

      // Local copy of nearby cells
      auto bgnd_array = background_t<array<CCTK_REAL, 4> >::load(
          bgnd_vars, idx - 2 * stride, stride);
      auto prim_array = primitive_t<array<CCTK_REAL, 4> >::load(
          prim_vars, idx - 2 * stride, stride);

      // Faces (cell boundaries):
      // [0: left boundary of this cell, 1: right boundary of left neightbour]

      // Reconstruct primitive variables at cell boundaries
      array<background_t<CCTK_REAL>, 2> bgnd;
      array<primitive_t<CCTK_REAL>, 2> prim;
      for (int face = 0; face < 2; ++face) {
        bgnd[face] = background_t<CCTK_REAL>::map(
            [&](const array<CCTK_REAL, 4> &vals) {
              return reconstruct(vals, face);
            },
            bgnd_array);
        prim[face] = primitive_t<CCTK_REAL>::map(
            [&](const array<CCTK_REAL, 4> &vals) {
              return reconstruct(vals, face);
            },
            prim_array);
      }

      // Calculate conserved variables at cell boundaries
      array<conserved_t<CCTK_REAL>, 2> cons;
      for (int face = 0; face < 2; ++face)
        cons[face] = conserved_t<CCTK_REAL>(bgnd[face], prim[face]);

      // Calculate fluxes at cell boundaries
      array<conserved_t<CCTK_REAL>, 2> flux;
      for (int face = 0; face < 2; ++face)
        flux[face] = cons[face].flux(bgnd[face], prim[face], dir);

      // Combine fluxes

      array<CCTK_REAL, 2> cs;
      for (int face = 0; face < 2; ++face)
        cs[face] = eos_cs(prim[face].rho, prim[face].eps);

      array<CCTK_REAL, 2> lambda;
      lambda[0] = fmin(CCTK_REAL(0), fmin(prim[0].vel[dir] - cs[0],
                                          prim[1].vel[dir] - cs[1]));
      lambda[1] = fmax(CCTK_REAL(0), fmax(prim[0].vel[dir] + cs[0],
                                          prim[1].vel[dir] + cs[1]));

      auto total_flux = conserved_t<CCTK_REAL>::map(
          [&](CCTK_REAL var_m, CCTK_REAL var_p, CCTK_REAL flux_m,
              CCTK_REAL flux_p) {
            return get_numeric_flux(lambda[0], lambda[1], var_m, var_p, flux_m,
                                    flux_p);
          },
          cons[0], cons[1], flux[0], flux[1]);

      total_flux.store(flux_vars[dir], idx);
    }
  }
  CCTK_ENDLOOP3_INT(NRHRSC_flux);
}

extern "C" void NRHRSC_flux_SelectBCs(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT nboundaryzones[6], is_internal[6], is_staggered[6], shiftout[6];
  int ierr = GetBoundarySpecification(6, nboundaryzones, is_internal,
                                      is_staggered, shiftout);
  if (ierr < 0)
    CCTK_ERROR("Internal error");
  for (int d = 1; d < 6; ++d)
    if (nboundaryzones[d] != nboundaryzones[0])
      CCTK_ERROR("Internal error");

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, nboundaryzones[0],
                                   -1, "NRHRSC::conserved_flux", flux_boundary);
  if (ierr < 0)
    CCTK_ERROR("Internal error");
}
}
